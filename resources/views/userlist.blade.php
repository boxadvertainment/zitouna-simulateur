@extends('layout')

@section('class', 'userlist')
@section('content')
    <div class="container">

        <h1>Tous les utilisateurs</h1>

        @if (Session::has('message'))

            <div class="alert alert-info">{{ Session::get('message') }}</div>

        @endif

        <table class="table table-striped table-bordered">

            <thead>

            <tr>

                <td>ID</td>

                <td>Nom</td>

                <td>Prenom</td>

                <td>Email</td>

                <td>Telephone</td>

                <td>Lien</td>

                <td>status</td>

                <td>Autofinancement</td>

                <td>Durée</td>

                <td>Puissance fiscale</td>

                <td>Age</td>

                <td>Prix</td>

                <td>Financement</td>

                <td></td>

            </tr>

            </thead>

            <tbody>

            @foreach($users as $key => $value)

                <tr>

                    <td>{{ $value->id }}</td>

                    <td>{{ $value->name }}</td>

                    <td>{{ $value->surname }}</td>

                    <td>{{ $value->email }}</td>

                    <td>{{ $value->telephone }}</td>

                    <td>{{ $value->lien }}</td>

                    <td>{{ $value->status }}</td>

                    <td>{{ $value->auto_financement }}</td>

                    <td>{{ $value->duree }}</td>

                    <td>{{ $value->puissance_fiscale }}</td>

                    <td>{{ $value->age }}</td>

                    <td>{{ $value->prix }}</td>

                    <td>{{ $value->financement }}</td>

                    <td>

                        <a class="btn btn-small btn-info" href="{{ URL::to('showUser/' . $value->id) }}">Modifier</a>

                        <a class="btn btn-small btn-danger" href="{{ URL::to('delete/' . $value->id) }}">Delete</a>

                    </td>

                </tr>

            @endforeach

            </tbody>

        </table>

    </div>
@endsection