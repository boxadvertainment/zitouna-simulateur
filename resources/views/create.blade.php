@extends('layout')

@section('class', 'home')

@section('content')
        {{--
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        --}}
                <form action="{{ action('AppController@store') }}" method="POST" class="form-horizontal" id="myForm" class="myForm">
                    {!! csrf_field() !!}
                    <div class="step1">
                        <div class="logo">
                            <img src="{{ url ('img/logo.png') }}" alt="Zitouna">
                        </div>
                        <div class="title">
                            <h3>Simulateur de financement</h3>
                            <div class="line"></div>
                        </div>
                        <div class="financement">
                            <h4>Type de financement</h4>
                            <span class="selectbox">
                                <select name="financement" id="financement" onchange="onSelectChange();" class="required" required="" data-parsley-group="block1">
                                    <option value="0">financement</option>
                                    <option value="standard">Standard</option>
                                    <option value="Tamouil_Sayara" slelected>Tamouil Sayara</option>
                                    <option value="Tamouil_Menzel">Tamouil Menzel</option>
                                    <option value="Tamouil_Akkarat_Afrad">Tamouil Akkarat El Afrad</option>
                                    <option value="Tamouil_moueddet_Ennakl">Tamouil Moueddet Ennakl</option>
                                    <option value="Tamouil_Mochtarayet">Tamouil Mochtarayet</option>
                                </select>
                            </span>
                        </div>
                        <div class="prix">
                            <img src="{{ url('img/icon1.png') }}">
                            <h4>Montant</h4>
                            <input type="text" name="prix" id="prix" class="" onchange="onChangePrice()" value="{{ Request::get('prix')  }}" required="" data-parsley-group="block1">
                            <h4 class="dt">DT</h4>
                        </div>
                        <div class="clearfix"></div>
                        <div id="ageId" class="age">
                            <img src="{{ url('img/icon2.png') }}">
                            <h4>Age de la voiture (ans)</h4>
                            <span><input type="radio" name="age" value="0" id="radio1" class="css-checkbox" <?php if (Request::get('age') == 0) echo 'checked'; ?>><label for="radio1" class="css-label">0</label></span>
                            <span><input type="radio" name="age" value="1" id="radio2" class="css-checkbox" <?php if (Request::get('age') == 1) echo 'checked'; ?>><label for="radio2" class="css-label">1</label></span>
                            <span><input type="radio" name="age" value="2" id="radio3" class="css-checkbox" <?php if (Request::get('age') == 2) echo 'checked'; ?>><label for="radio3" class="css-label">2</label></span>
                            <span><input type="radio" name="age" value="3" id="radio4" class="css-checkbox" <?php if (Request::get('age') == 3) echo 'checked'; ?>><label for="radio4" class="css-label">3</label></span>
                        </div>
                        <div class="clearfix"></div>
                        <div id="puissanceFiscaleId" class="puissance">
                            <img src="{{ url('img/icon3.png') }}">
                            <h4>Puissance Fiscale</h4>
                            <span><input type="radio" name="puissance_fiscale" value="4" id="radio5" class="css-checkbox" onchange="onChangeNombreCheveaux1()" <?php if (Request::get('puissance_fiscale') == 4) echo 'checked'; ?> ><label for="radio5" class="css-label">4CV</label></span>
                            <span><input type="radio" name="puissance_fiscale" value="5" id="radio6" class="css-checkbox" onchange="onChangeNombreCheveaux2()" <?php if (Request::get('puissance_fiscale') == 5) echo 'checked'; ?>><label for="radio6" class="css-label">5CV ou plus</label></span>
                        </div>
                        <div class="clearfix"></div>
                        <div id="dureeId" class="duree">
                            <img src="{{ url('img/icon4.png') }}">
                            <h4>Durée</h4>
                            <input id="dureeslider" type="text" name="duree" data-provide="slider">
                        </div>
                        <div class="autofinancement">
                            <img src="{{ url('img/icon5.png') }}">
                            <h4>Autofinancement</h4>
                            <input type="text" name="auto_financement" id="auto_financement" onchange="onChangeValeurPropre()" class="required" required="" data-parsley-group="block1">
                            <h4 class="dt">DT</h4>
                        </div>
                        <div class="clearfix"></div>

                        <div class="footer-wrapper">
                            <div class="mensualiteClass">
                                <h3><i class="fa fa-play" aria-hidden="true"></i>
                                    Mensualité : <span id="mensualite">-</span></h3>
                            </div>
                            <div class="text">
                                <h4> Montant Emprunté : <span id="montant1">-</span> dt</h4>
                                <h4> Durée : <span id="durespan1"></span> ans</h4>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn_calc" onclick="switchtab(1)"> Calculer </button>
                            </div>
                        </div>

                    </div>

                    <div class="step2 hide">
                        <div class="logo">
                            <img src="{{ url ('img/logo.png') }}" alt="Zitouna">
                        </div>
                        <div class="title">
                            <h4>Remplissez le formulaire pour pouvoir vous contacter</h4>
                            <div class="line"></div>
                        </div>
                        <div class="name">
                            <img src="{{ url('img/icon6.png') }}">
                            <h4>Nom :</h4>
                            <input type="text" name="name" id="name" class="form-control required" required="" data-parsley-group="block2" title="Nom incorrect" placeholder="Nom *">
                        </div>
                        <div class="clearfix"></div>
                        <div class="surname">
                            <img src="{{ url('img/icon6.png') }}">
                            <h4>Prénom :</h4>
                            <input type="text" name="surname" id="surname" class="form-control required" required="" data-parsley-group="block2" title="Prénom incorrect" placeholder="Prénom *">
                        </div>
                        <div class="clearfix"></div>
                        <div class="email">
                            <img src="{{ url('img/icon7.png') }}">
                            <h4>Email :</h4>
                            <input type="text" name="email" id="email" class="form-control required" data-parsley-type="email" placeholder="Email *"  title="Email incorrect" required="" data-parsley-group="block2">
                        </div>
                        <div class="clearfix"></div>
                        <div class="telephone">
                            <img src="{{ url('img/icon8.png') }}">
                            <h4>Téléphone :</h4>
                            <input type="text" name="telephone" id="telephone" class="form-control required" maxlength="8" placeholder="Téléphone *" maxlength="8" required="" data-parsley-group="block2" pattern="^[0-9]{8}$" title="Téléphone incorrect">
                        </div>
                        <div class="clearfix"></div>
                        <div class="divBTN">
                            <button type="button" class="validate-btn" onclick="switchtab(2)"> Valider </button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="footer-wrapper">
                            <div class="mensualiteClass">
                                <h3><i class="fa fa-play" aria-hidden="true"></i>
                                    Mensualité : <span id="mensualite_step2">-</span></h3>
                            </div>
                            <div class="text">
                                <h4> Montant Emprunté : <span id="montant2">-</span> dt</h4>
                                <h4> Durée : <span id="durespan2"></span> ans</h4>
                            </div>
                        </div>
                    </div>
                    <div class="step3 hide">
                        <div class="logo">
                            <img src="{{ url ('img/logo.png') }}" alt="Zitouna">
                        </div>
                        <div class="title">
                            <h3>Merci</h3>
                            <div class="line"></div>
                            <h5>Nous vous recontacterons bientôt.</h5>
                        </div>
                        <div class="divBTN">
                            <button type="button" class="return-btn" onclick="showPage('.step1')"> Retrouver un autre simulation </button>
                        </div>
                        <div class="mensualiteClass">
                            <h3><i class="fa fa-play" aria-hidden="true"></i>
                                Mensualité : <span id="mensualite_step3">-</span></h3>
                        </div>
                        <div class="text">
                            <h4> Montant Emprunté : <span id="montant3">-</span> dt</h4>
                            <h4> Durée : <span id="durespan3"></span> ans</h4>
                        </div>
                    </div>
                </form>
@endsection
@section('scripts')
    <script type="text/javascript">
        function switchtab(index){
            if (index == 1){
                if ($('#myForm').parsley().validate({group: 'block1'}))
                    showPage('.step2');
            }
            if (index == 2){
                if ($('#myForm').parsley().validate({group: 'block2'})){
                        var $this = $('#myForm');
                        $.ajax({
                            url: $this.attr('action'),
                            method: 'POST',
                            data: $this.serialize()
                        }).done(function(response){
                            if(response.success) {
                                showPage('.step3');
                            }else{
                                showPage('.step1');
                            }
                        });
                }
            }
        };

        function showPage (selector) {
            $(selector).removeClass('hide').siblings().addClass('hide');
        }

        $(document).ready(function() {

            $('#myForm').parsley({
                errorsContainer: function (ParsleyField) {
                    return ParsleyField.$element.attr("title");
                },
                errorsWrapper: false
            });
            $.listen('parsley:field:error', function(fieldInstance) {
                var messages = ParsleyUI.getErrorsMessages(fieldInstance);
                fieldInstance.$element.tooltip('destroy');
                fieldInstance.$element.tooltip({
                    animation: true,
                    container: 'body',
                    placement: 'top',
                    title: messages
                });
            });
            $.listen('parsley:field:success', function(fieldInstance) {
                fieldInstance.$element.tooltip('destroy');
            });
        });

        // function return requests parameter
        function getRequests() {
            var s1 = location.search.substring(1, location.search.length).split('&'),
                    r = {}, s2, i;
            for (i = 0; i < s1.length; i += 1) {
                s2 = s1[i].split('=');
                r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
            }
            return r;
        };
        var requestParams = getRequests();

        var data = '{"data_type":"json","typeFinancement":[' +
                '{"type":"Tamouil_Sayara",' +
                    '"values":[' +
                        '{"name":"default","plafond":100000,"Apport_propre":20,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"voiture","cv":4,"plafond":100000,"Apport_propre":20,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"voiture","cv":5,"plafond":100000,"Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"motos","cv":4,"plafond":100000,"Apport_propre":20,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"motos","cv":5,"plafond":100000,"Apport_propre":40,"dureeMin":1,"dureeMax":7}' +
                '],' +
                    '"marge":{"1":"4.66","2":"4.55","3":"4.55","4":"4.58","5":"4.62","6":"4.82","7":"4.87"}},' +
                '{"type":"Tamouil_moueddet_Ennakl",' +
                    '"values":[' +
                        '{"name":"default","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"Tracteurs-véhicules-agricoles","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"Fourgonettes","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                        '{"name":"Camions-véhicules-industriels","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7}' +
                '],' +
                    '"marge":{"1":"4.39","2":"4.27","3":"4.27","4":"4.30","5":"4.33","6":"4.37","7":"4.42"}},' +
                '{"type":"Tamouil_Mochtarayet",' +
                    '"values":[' +
                        '{"name":"default","plafond":15000,"Apport_propre":0,"dureeMin":1,"dureeMax":3},' +
                        '{"name":"Motos-sans-Carte-Grise","plafond":15000,"Apport_propre":0,"dureeMin":1,"dureeMax":3}' +
                '],' +
                    '"marge":{"1":"4.80","2":"4.68","3":"4.69"}},' +
                '{"type":"standard",' +
                    '"values":[' +
                        '{"name":"Bateaux"},' +
                        '{"name":"Autres-véhicules"},' +
                        '{"name":"Pièces-rechange-accessoires"},' +
                        '{"name":"Colocations"}' +
                    ']},' +
                '{"type":"Tamouil_Menzel",' +
                    '"values":[' +
                        '{"name":"default","plafond":"0","Apport_propre":20,"dureeMin":7,"dureeMax":15},' +
                        '{"name":"Appartement","plafond":"0","Apport_propre":20,"dureeMin":7,"dureeMax":15},' +
                        '{"name":"Maison","plafond":"0","Apport_propre":20,"dureeMin":7,"dureeMax":15},' +
                        '{"name":"Immobilier-de-vacances","plafond":"0","Apport_propre":20,"dureeMin":7,"dureeMax":15},' +
                        '{"name":"terrains","plafond":"0","Apport_propre":20,"dureeMin":7,"dureeMax":15}' +
                    '],' +
                    '"marge":{"7":"4.27","8":"4.46","9":"4.51","10":"4.56","11":"4.77","12":"4.82","13":"4.87","14":"4.92","15":"4.98"}},' +
                '{"type":"Tamouil_Akkarat_Afrad",' +
                    '"values":[' +
                        '{"name":"default","plafond":"-","Apport_propre":30,"dureeMin":2,"dureeMax":7},' +
                        '{"name":"Bureaux-locaux-commerciaux","plafond":"0","Apport_propre":30,"dureeMin":2,"dureeMax":7},' +
                        '{"name":"Autre-Immobilier","plafond":"0","Apport_propre":30,"dureeMin":2,"dureeMax":7}' +
                '],' +
                    '"marge":{"2":"4.00","3":"3.99","4":"4.01","5":"4.05","6":"4.23","7":"4.27"}}' +
                ']}';

        var jsonData = JSON.parse(data);
        var DureeMin = 0;
        var DureeMax = 0;
        for (var i = 0; i < jsonData.typeFinancement.length; i++) {
            for (var j = 0; j < jsonData.typeFinancement[i].values.length; j++) {
                if (requestParams["categorie"] == jsonData.typeFinancement[i].values[j].name){
                    if (jsonData.typeFinancement[i].type != 'Tamouil_Sayara'){
                        $("#ageId").hide();
                        $("#puissanceFiscaleId").hide();
                    }
                    $("#financement").val(jsonData.typeFinancement[i].type);
                    DureeMin = parseInt(jsonData.typeFinancement[i].values[j].dureeMin);
                    DureeMax = parseInt(jsonData.typeFinancement[i].values[j].dureeMax);
                }
            }
        }

        // remplir le tableau duree pour le slider
        var array_thick = [];
        var index = 0;
        for (var k = DureeMin; k < DureeMax + 1; k++) {
            array_thick[index] = k;
            index++;
        }
        $("#dureeslider").slider({
            ticks:          array_thick,
            ticks_labels:   array_thick,
            min:            1,
            max:            array_thick.length,
            step:           1,
            value:          array_thick.length
        });
        //var montant1 = document.querySelector("#montant1");
        //var montant2 = document.querySelector("#montant2");
        //var montant3 = document.querySelector("#montant3");
        //montant1.textContent = $("#prix").val();
        //montant2.textContent = $("#prix").val();
        //montant3.textContent = $("#prix").val();

        //var dureespan1 = document.querySelector("#dureespan1");
        //var dureespan2 = document.querySelector("#dureespan2");
        //var dureespan3 = document.querySelector("#dureespan3");

        //dureespan1.textContent =  $("#dureeslider").slider('getValue');
        //dureespan1.textContent = $("#dureeslider").slider('getValue');
        //dureespan1.textContent = $("#dureeslider").slider('getValue');

        function onSelectChange(){

            var financement = $("#financement").val();
            if (financement != 'Tamouil_Sayara'){
                $("#ageId").hide();
                $("#puissanceFiscaleId").hide();
            }else{
                $("#ageId").show();
                $("#puissanceFiscaleId").show();
            }

            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                if ( (financement == jsonData.typeFinancement[i].type) && (financement != 'standard') ){
                    var default_value = jsonData.typeFinancement[i].values[0];
                    var min = default_value["dureeMin"];
                    var max = default_value["dureeMax"];
                    var array_thick2 = [];
                    var array_thick_labels = [];
                    var index2 = 0;
                    for (var k = min; k < max + 1; k++) {
                        array_thick2[index2] = k;
                        array_thick_labels[index2] = k + ' ans';
                        index2++;
                    }
                    $("#dureeslider").slider('destroy');
                    $("#dureeslider").slider({
                        ticks:          array_thick2,
                        ticks_labels:   array_thick2,
                        min:            1,
                        max:            array_thick2.length,
                        step:           1,
                        value:          array_thick2.length
                    });

                    //dureespan1.textContent =  $("#dureeslider").slider('getValue');
                    //dureespan2.textContent =  $("#dureeslider").slider('getValue');
                    //dureespan3.textContent =  $("#dureeslider").slider('getValue');
                }
            }
        }
        var plafond = 0;
        function Apport_propre (param){
            var apport = 0;
            var valeur = 0;
            var default_valeur = 0;
            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                for (var j = 0; j < jsonData.typeFinancement[i].values.length; j++) {
                    if (jsonData.typeFinancement[i].type == "Tamouil_Sayara"){
                        var nbCV = requestParams["puissance_fiscale"];
                        if ( (param == jsonData.typeFinancement[i].values[j].name) && (jsonData.typeFinancement[i].values[j].cv == nbCV) ) {
                            apport = parseInt(jsonData.typeFinancement[i].values[j].Apport_propre);
                            valeur = ( apport * parseInt($("#prix").val())) / 100;
                            $("#auto_financement").val(valeur);
                            plafond = parseInt(jsonData.typeFinancement[i].values[j].plafond);
                        }
                    }else{
                        if (param == jsonData.typeFinancement[i].values[j].name) {
                            plafond = parseInt(jsonData.typeFinancement[i].values[j].plafond);
                            if (parseInt(jsonData.typeFinancement[i].values[j].Apport_propre) > 0) {
                                apport = parseInt(jsonData.typeFinancement[i].values[j].Apport_propre);
                                valeur = ( apport * parseInt($("#prix").val())) / 100;
                                $("#auto_financement").val(valeur);
                            } else {
                                $("#auto_financement").val(default_valeur);
                            }
                        }
                    }
                }
            }
        }

        Apport_propre(requestParams["categorie"]); //Appel du fonction Apport_propre.
        var ApportPropre = $("#auto_financement").val(); //initialiser la valeur ApportPropre pour le tester avec le nouvel valeur ajouté par l'utilisateur
        var prix = $("#prix").val();

        /* fonction qui test la valeur ajouté par l'utilisateur par rapport a la valeur exigé  */
        function onChangeValeurPropre() {
            var nouvelleApport = $("#auto_financement").val();
            var prix = $("#prix").val();
            if ( ApportPropre != 0 ){
                if ( nouvelleApport < ApportPropre ){
                    $("#auto_financement").val(ApportPropre);
                }
                if ( nouvelleApport >= prix ){
                    $("#auto_financement").val(ApportPropre);
                }
            }
        }

        function onChangeNombreCheveaux1(){
            var apport = 20;
            var valeur = ( apport * parseInt($("#prix").val())) / 100;
            $("#auto_financement").val(valeur);
        }
        function onChangeNombreCheveaux2(){
            var apport = 40;
            var valeur = ( apport * parseInt($("#prix").val())) / 100;
            $("#auto_financement").val(valeur);
        }

        function onChangePrice (){
            var nouveauPrix = $("#prix").val();
            if ( nouveauPrix > plafond){
                $("#prix").val(requestParams["prix"]);
            }
        }

        function marge_profit_annualise (param) {
            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                if ( param == jsonData.typeFinancement[i].type ) {
                    if (param != "standard"){
                        var marge_index = $("#dureeslider").val();
                        var marge = jsonData.typeFinancement[i].marge[marge_index];
                    }
                }
            }
            return parseFloat(marge).toFixed(2);
        }

        function calculer_mensualite () {
            var montant_financement = parseInt($("#prix").val()) - parseInt($("#auto_financement").val());

            var param = $("#financement").val();
            var marge_globale = (marge_profit_annualise(param)/100) * parseInt($("#dureeslider").val());

            var mensualite = ( montant_financement * ( 1 + marge_globale )) / ( parseInt($("#dureeslider").val()) * 12 );
            return parseFloat(mensualite).toFixed(3);
        }

        var message_mensualite = document.querySelector("#mensualite");
        var message_mensualite2 = document.querySelector("#mensualite_step2");
        var message_mensualite3 = document.querySelector("#mensualite_step3");

        message_mensualite.textContent = calculer_mensualite();
        message_mensualite2.textContent = calculer_mensualite();
        message_mensualite3.textContent = calculer_mensualite();

        $("#myForm").change(function (){
            message_mensualite.textContent  = calculer_mensualite();
            message_mensualite2.textContent = calculer_mensualite();
            message_mensualite3.textContent = calculer_mensualite();

            /*
            montant1.textContent = $("#prix").val();
            montant2.textContent = $("#prix").val();
            montant3.textContent = $("#prix").val();

            dureespan1.textContent = $("#dureeslider").slider('getValue');
            dureespan2.textContent = $("#dureeslider").slider('getValue');
            dureespan3.textContent = $("#dureeslider").slider('getValue');
            */

        });

    </script>
    @endsection