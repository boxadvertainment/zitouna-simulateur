@extends('layout')

@section('class', 'home')

@section('content')
    <div class="container">

       <h1>Modify User</h1>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <form action="{{ action('AppController@update', ['id' => $user->id]) }}" method="POST" class="form-horizontal">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="name">Nom:</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$user->name}}">
                    </div>
                    <div class="form-group">
                        <label for="surname">Prénom :</label>
                        <input type="text" name="surname" id="surname" class="form-control" value="{{$user->surname}}">
                    </div>
                    <div class="form-group">
                        <label for="lien">Lien :</label>
                        <input type="text" name="lien" id="lien" class="form-control" value="{{$user->lien}}">
                    </div>
                    <div class="form-group">
                        <label for="prix">Prix :</label>
                        <input type="text" name="prix" id="prix" class="form-control" value="{{$user->prix}}">
                    </div>
                    <div class="form-group">
                        <label for="duree">Durée :</label>
                        <input type="text" name="duree" id="duree" class="form-control" value="{{$user->duree}}">
                    </div>
                    <div class="form-group">
                        <label for="telephone">Téléphone :</label>
                        <input type="text" name="telephone" id="telephone" class="form-control" value="{{$user->telephone}}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email :</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{$user->email}}">
                    </div>
                    <div class="form-group">
                        <label for="age">Age :</label>
                        <input type="text" name="age" id="age" class="form-control" value="{{$user->age}}">
                    </div>
                    <div class="form-group">
                        <label for="puissance_fiscale">Puissance Fiscale :</label>
                        <input type="text" name="puissance_fiscale" id="puissance_fiscale" class="form-control" value="{{$user->puissance_fiscale}}">
                    </div>
                    <div class="form-group">
                        <label for="financement">Financement :</label>
                        <input type="text" name="financement" id="financement" class="form-control" value="{{$user->financement}}">
                    </div>
                    <div class="form-group">
                        <label for="auto_financement">Auto Financement :</label>
                        <input type="text" name="auto_financement" id="auto_financement" class="form-control" value="{{$user->auto_financement}}">
                    </div>
                    <div class="form-group">
                        <label for="status">Status :</label>
                        <input type="text" name="status" id="status" class="form-control" value="{{$user->status}}">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
