@extends('layout')

@section('class', 'home')

@section('content')
    <div class="container">
        <h1 class="text-center">Detail pour le client {{$user->name_surname}}</h1>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped table-bordered">
                    <tbody>
                    <tr>
                        <td><label for="name">Nom :</label></td>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <td><label for="surname">Prénom :</label></td>
                        <td>{{$user->surname}}</td>
                    </tr>
                    <tr>
                        <td><label for="lien">Lien :</label></td>
                        <td>{{$user->lien}}</td>
                    </tr>
                    <tr>
                        <td><label for="prix">Prix :</label></td>
                        <td>{{$user->prix}}</td>
                    </tr>
                    <tr>
                        <td><label for="duree">Durée :</label></td>
                        <td>{{$user->duree}}</td>
                    </tr>
                    <tr>
                        <td><label for="telephone">Téléphone :</label></td>
                        <td>{{$user->telephone}}</td>
                    </tr>
                    <tr>
                        <td><label for="email">Email :</label></td>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <td><label for="age">Age :</label></td>
                        <td>{{$user->age}}</td>
                    </tr>
                    <tr>
                        <td><label for="puissance_fiscale">Puissance Fiscale :</label></td>
                        <td>{{$user->puissance_fiscale}}</td>
                    </tr>
                    <tr>
                        <td><label for="financement">Financement :</label></td>
                        <td>{{$user->financement}}</td>
                    </tr>
                    <tr>
                        <td><label for="auto_financement">Auto Financement :</label></td>
                        <td>{{$user->auto_financement}}</td>
                    </tr>
                    <tr>
                        <td><label for="status">Status :</label></td>
                        <td>{{$user->status}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <input class="btn btn-primary" type="button" value="Retour" onclick="history.go(-1)">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
