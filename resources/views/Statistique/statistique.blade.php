@extends('layout')

@section('class', 'home')

@section('content')
    <div class="container">
        <h1 class="text-center">Statistique Générale</h1>
        <div class="row">
            <div class="col-sm-3">
                <div class="hero-widget well well-sm">
                    <div class="icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="text">
                        <var>{{ $nbsayara }}</var>
                        <label class="text-muted">Tamouil Sayara</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="hero-widget well well-sm">
                    <div class="icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="text">
                        <var>{{ $nbennakl }}</var>
                        <label class="text-muted">Tamouil Moueddet Ennakl</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="hero-widget well well-sm">
                    <div class="icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="text">
                        <var>{{ $nbmochtarayet }}</var>
                        <label class="text-muted">Tamouil Mochtarayet</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="hero-widget well well-sm">
                    <div class="icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                    <div class="text">
                        <var>{{ $nbmanzel }}</var>
                        <label class="text-muted">Tamouil Menzel</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-6">
                <h1>Financement par type</h1>
                <p>Nombre de financement totale : {{ $total }}</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Sayara </strong>: {{ $sayara }} %</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Moueddet Ennakl </strong>: {{ $ennakl }} %</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Mochtarayet </strong>: {{ $mochtarayet }} %</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Menzel </strong>: {{ $menzel }} %</p>
            </div>
            <div class="col-sm-6 col-md-4">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Financement par montant</h1>
                <p>Chiffre totale de financement : {{ $montant }}</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Sayara </strong>: {{ number_format($plage1, 2) }} % par rapport au montant totale</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Moueddet Ennakl </strong>: {{ number_format($plage2, 2) }} % par rapport au montant totale</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Mochtarayet </strong>: {{ number_format($plage3, 2) }} % par rapport au montant totale</p>
                <p>Le pour pourcentage de financement <strong> Tamouil Menzel </strong>: {{ number_format($plage4, 2) }} % par rapport au montant totale</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Graphique d'evolution</h1>
                <p>Nombre de clics sur le simulateur : <strong>en attente</strong></p>
                <p>Nombre d'utilisateur qui on tester le simulateur  : <strong> en attente </strong></p>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{ asset('js/chart/Chart.js') }}"></script>
    <script type="text/javascript">
        var data = {
            labels: [
                "Tamouil Sayara",
                "Tamouil Moueddet Ennakl",
                "Tamouil Mochtarayet",
                "Tamouil Menzel"
            ],
            datasets: [
                {
                    data: [{{ $nbsayara }}, {{ $nbennakl }}, {{ $nbmochtarayet }}, {{ $nbmanzel }}],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#D0288D"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#D0288D"
                    ]
                }]
        };

        var ctx = document.getElementById("myChart");

        var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: data
        });

    </script>

@endsection