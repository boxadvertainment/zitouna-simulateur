@extends('layout')

@section('class', 'home')

@section('content')
    <div class="container" id="dvData">
        <h1>Tous les utilisateurs</h1>
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li><a href="{{ URL::to('/listing') }}" data-toggle="tab">Tous Catégorie</a></li>
                    <li><a href="{{ URL::to('/listing/sayara') }}" data-toggle="tab">Tamouil Sayara</a></li>
                    <li><a href="{{ URL::to('/listing/ennakl') }}" data-toggle="tab">Tamouil Moueddet Ennakl</a></li>
                    <li><a href="{{ URL::to('/listing/mochtarayet') }}" data-toggle="tab">Tamouil Mochtarayet</a></li>
                    <li class="active"><a data-toggle="tab">Tamouil Menzel</a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab5default">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nom</td>
                                <td>Prénom</td>
                                <td>Email</td>
                                <td>Telephone</td>
                                <td>Lien</td>
                                <td>Autofinancement</td>
                                <td>Prix</td>
                                <td>Financement</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $value)
                                <tr>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->surname }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->telephone }}</td>
                                    <td>{{ $value->lien }}</td>
                                    <td>{{ $value->auto_financement }}</td>
                                    <td>{{ $value->prix }}</td>
                                    <td>{{ $value->financement }}</td>
                                    <td>
                                        {{--<a class="btn btn-small btn-info" href="{{ URL::to('showUser/' . $value->id) }}">Modifier</a>--}}
                                        {{--<a class="btn btn-small btn-danger" href="{{ URL::to('delete/' . $value->id) }}">Delete</a>--}}
                                        <a class="btn btn-small btn-info" href="{{ URL::to('detail/' . $value->id) }}">
                                            Detail
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="export" style="margin-right: 100px">Exporter la table en CSV</a>
        <a href="{{ URL::to('stat') }}">Voir statistique</a>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function () {

            function exportTableToCSV($table, filename) {

                var $rows = $table.find('tr:has(td)'),

                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                        tmpColDelim = String.fromCharCode(11), // vertical tab character
                        tmpRowDelim = String.fromCharCode(0), // null character

                // actual delimiter characters for CSV format
                        colDelim = '","',
                        rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                        csv = '"' + $rows.map(function (i, row) {
                                    var $row = $(row),
                                            $cols = $row.find('td');

                                    return $cols.map(function (j, col) {
                                        var $col = $(col),
                                                text = $col.text();

                                        return text.replace(/"/g, '""'); // escape double quotes

                                    }).get().join(tmpColDelim);

                                }).get().join(tmpRowDelim)
                                        .split(tmpRowDelim).join(rowDelim)
                                        .split(tmpColDelim).join(colDelim) + '"',

                // Data URI
                        csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                $(this)
                        .attr({
                            'download': filename,
                            'href': csvData,
                            'target': '_blank'
                        });
            }

            // This must be a hyperlink
            $(".export").on('click', function (event) {
                // CSV
                exportTableToCSV.apply(this, [$('#dvData>table'), 'export.csv']);

                // IF CSV, don't do event.preventDefault() or return false
                // We actually need this to be a typical hyperlink
            });
        });

    </script>
@endsection