var elixir   = require('laravel-elixir');

require('./elixir-extensions');

elixir(function(mix) {
    mix
        .sass('main.scss', 'public/css/main.css')
        .babel([
            //'facebookUtils.js',
            'main.js'
        ], 'public/js/main.js')
        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
            'bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.js',
            'bower_components/parsleyjs/dist/parsley.js',
            'bower_components/Chart.js/dist/Chart.js'
            // endbower
        ], 'public/js/vendor.js', 'bower_components')
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css'
            //'bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.css',
            // endbower
        ], 'public/css/vendor.css', 'bower_components')
        .copy('bower_components/font-awesome/fonts', 'public/fonts')
        //.version(['css/main.css', 'js/main.js'])
        //.copy('public/fonts', 'public/build/fonts')
        .images()
        .wiredep()
        .minifyCss()
        .compress()
        .browserSync({
            proxy: process.env.APP_URL
        });
});
