<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatistiqueController extends Controller
{
    //
    public function listing()
    {
//        User::create([
//            'name_surname' => 'Mohamed',
//            'email' => 'majhoul@gmail.com',
//            'financement' => 'Tamouil Sayara',
//            'age' => '3',
//            'puissance_fiscale' => '5',
//            'duree' => '2',
//            'auto_financement' => '2500',
//            'telephone' => '20441505',
//            'lien' => 'https://www.facebook.com',
//            'status' => 'ok',
//            'prix' => '78500',
//        ]);
        $users = User::all();
        return view('Statistique.liste')->with('users', $users);
    }

    public function detail($id)
    {
        $user = User::find($id);
        return view('Statistique.detail')->with('user', $user);
    }

    public function listsayara()
    {
        $users = User::where('financement', '=', 'Tamouil_Sayara')->get();
        return view('Statistique.sayara')->with('users', $users);
    }

    public function listennakl()
    {
        $users = User::where('financement', '=', 'Tamouil_Moueddet_Ennakl')->get();
        return view('Statistique.ennakl')->with('users', $users);
    }

    public function listmochtarayet()
    {
        $users = User::where('financement', '=', 'Tamouil_Mochtarayet')->get();
        return view('Statistique.mochtarayet')->with('users', $users);
    }

    public function listmenzel()
    {
        $users = User::where('financement', '=', 'Tamouil_Menzel')->get();
        return view('Statistique.menzel')->with('users', $users);
    }

    public function stat()
    {

        //financement par type
        $total= User::all()->count();
        $nbsayara = User::where('financement', '=', 'Tamouil_Sayara')->count();
        $sayara = (100 / $total) * $nbsayara;
        $nbennakl = User::where('financement', '=', 'Tamouil_Moueddet_Ennakl')->count();
        $ennakl = (100 / $total) * $nbennakl;
        $nbmochtarayet = User::where('financement', '=', 'Tamouil_Mochtarayet')->count();
        $mochtarayet = (100 / $total) * $nbmochtarayet;
        $nbmanzel = User::where('financement', '=', 'Tamouil_Menzel')->count();
        $menzel = (100 / $total) * $nbmanzel;

        //financement par Montant
        $montant = User::sum('prix');
        $plage1 = (100 / ($montant/User::where('financement', '=', 'Tamouil_Sayara')->sum('prix')));
        $plage2 = (100 / ($montant/User::where('financement', '=', 'Tamouil_Moueddet_Ennakl')->sum('prix')));
        $plage3 = (100 / ($montant/User::where('financement', '=', 'Tamouil_Mochtarayet')->sum('prix')) );
        $plage4 = (100 / ($montant/User::where('financement', '=', 'Tamouil_Menzel')->sum('prix')));

        return view('Statistique.statistique')->with([
            'nbsayara' => $nbsayara,
            'nbennakl' => $nbennakl,
            'nbmochtarayet' => $nbmochtarayet,
            'nbmanzel' => $nbmanzel,
            'total' => $total,
            'sayara' => $sayara,
            'ennakl' => $ennakl,
            'mochtarayet' => $mochtarayet,
            'menzel' => $menzel,
            'montant' => $montant,
            'plage1' => $plage1,
            'plage2' => $plage2,
            'plage3' => $plage3,
            'plage4' => $plage4
        ]);
    }
}
