<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AppController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function  home(){
        return view('index');
    }

    public function  home2(){
        return view('create');
    }

    public function index(){
        $users = User::all();
        return View::make('userlist')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request){

        $validator = \Validator::make($request->all(), [
            'name'  =>  'required',
            'surname'  =>  'required',
            'email' =>  'required|email',
            'telephone' =>  'required|regex:/^[0-9]{8}$/',
            'prix'  =>  'required|integer',
            //'lien'  =>  'required|url',
            //'status'  =>  'required',
            'auto_financement'  =>  'required|integer',
            //'duree'  =>  'required|integer|between:1,7',
            'puissance_fiscale'  =>  'required|integer|between:4,5',
            //'age'  =>  'required|integer|between:0,3',
            'financement'  =>  'required',
        ]);
        //
        // process the login
        if ($validator->fails()) {
            //return redirect()->back()->withErrors($validator)->withInput();
            return response()->json(['success' => false]);
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->email = $request->email;
            $user->telephone = $request->telephone;
            $user->prix = $request->prix;
            $user->lien = $_SERVER['HTTP_REFERER'];
            //$user->status = $request->status;
            $user->auto_financement = $request->auto_financement;
            $user->duree = $request->duree;
            $user->financement = $request->financement;
            if ($request->financement == 'Tamouil_Sayara'){
                $user->puissance_fiscale = $request->puissance_fiscale;
                $user->age = $request->age;
            }else{
                $user->puissance_fiscale = null;
                $user->age = null;
            }

            $user->save();
            Session::flash('message', 'Successfully Added user!');
            //return Redirect::to('/');
            return response()->json(['success' => true]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        $user = User::find($id);
        return View::make('edit')->with('user', $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request){
         $messages = [
            'required' => 'The :attribute field is required.',
             'email' => 'The :attribute must be an  email.',
             'integer' => 'The :attribute must be an integer.',
        ];
        $validator = \Validator::make($request->all(), [
            'name'  =>  'required',
            'surname'  =>  'required',
            'email' =>  'required|email',
            'telephone' =>  'required|regex:/^[0-9]{8}$/',
            'prix'  =>  'required|integer',
            'lien'  =>  'required|url',
            'status'  =>  'required',
            'auto_financement'  =>  'required|integer',
            'duree'  =>  'required|integer|between:1,7',
            'puissance_fiscale'  =>  'required|integer|between:4,5',
            'age'  =>  'required|integer|between:0,3',
            'financement'  =>  'required',
        ], $messages);
        // process the login
        if ($validator->fails()) {
            return Redirect::to('/showUser/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $user = User::find($id);
            $user->name       = $request->name;
            $user->surname       = $request->surname;
            $user->email      = $request->email;
            $user->telephone = $request->telephone;
            $user->prix = $request->prix;
            $user->lien = $request->lien;
            $user->status = $request->status;
            $user->auto_financement = $request->auto_financement;
            $user->duree = $request->duree;
            $user->puissance_fiscale = $request->puissance_fiscale;
            $user->age = $request->age;
            $user->financement = $request->financement;
            $user->save();

            Session::flash('message', 'Successfully updated user!');
            //return Redirect::to('/');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        Session::flash('message', 'Successfully deleted the user!');
        //return Redirect::to('/');
        return redirect()->back();
    }

}
