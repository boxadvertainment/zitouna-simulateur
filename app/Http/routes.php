<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'AppController@home');
    Route::get('/create', 'AppController@home2');
    Route::get('/list', 'AppController@index');
    Route::post('/add', 'AppController@store');
    Route::get('/showUser/{id}', 'AppController@show');
    Route::post('/update/{id}', 'AppController@update');
    Route::get('/delete/{id}', 'AppController@destroy');

    Route::get('/listing', 'StatistiqueController@listing');
    Route::get('/listing/sayara', 'StatistiqueController@listsayara');
    Route::get('/listing/ennakl', 'StatistiqueController@listennakl');
    Route::get('/listing/mochtarayet', 'StatistiqueController@listmochtarayet');
    Route::get('/listing/menzel', 'StatistiqueController@listmenzel');
    
    Route::get('/detail/{id}', 'StatistiqueController@detail');
    Route::get('/stat', 'StatistiqueController@stat');
});