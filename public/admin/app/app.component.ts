import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PageComponent} from './pages/page.component';

@Component({
    selector: 'app',
    templateUrl: './app/app.html',
    directives: [ ROUTER_DIRECTIVES ]
})
@RouteConfig([
  {path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault: true},
  {path: '/pages/...', name: 'Pages', component: PageComponent},
  {path: '/disaster', name: 'Asteroid', redirectTo: ['CrisisCenter', 'CrisisDetail', {id:3}]},
])

export class AppComponent {
}
