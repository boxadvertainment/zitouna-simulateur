import {Component, OnInit, AfterViewInit}  from 'angular2/core';
import {Page, PageService} from './page.service';
import {RouteParams, Router} from 'angular2/router';

declare var $: any;

@Component({
  templateUrl: './app/pages/page-detail.html',
})
export class PageDetailComponent implements AfterViewInit {
  public page: Page;
  constructor(
    private _service: PageService,
    private _router: Router,
    private _routeParams: RouteParams
    ) { }
    
  ngAfterViewInit(){
      setTimeout(function(){
        $('.summernote').summernote();
      }, 3000);
  }

  ngOnInit() {
    let id = +this._routeParams.get('id');
    this._service.getPage(id).subscribe(page => {
      if (page) {
        this.page = page;
      } else { // id not found
        this.gotoPages();
      }
    });
  }

  gotoPages() {
    this._router.navigate(['Pages']);
  }
  save() {
      
  }
}
