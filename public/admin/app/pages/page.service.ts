import {Injectable} from 'angular2/core';
import 'rxjs/Rx';
import {Http} from 'angular2/http';
export class Page {
  constructor(public id: number, public name: string) { }
}
@Injectable()
export class PageService {
  constructor(private http: Http) {
  }
  getPages() {
    return this.http.get('http://starter-site.dev/admin/pages').map(res => res.json());
  }
  getPage(id: number | string) {
    return this.http.get('http://starter-site.dev/admin/page/' + id).map(res => res.json());
  }
}
