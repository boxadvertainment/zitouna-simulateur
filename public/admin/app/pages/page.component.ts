import {Component}     from 'angular2/core';
import {RouteConfig, RouterOutlet} from 'angular2/router';
import {PageListComponent}   from './page-list.component';
import {PageDetailComponent} from './page-detail.component';
import {PageService}         from './page.service';
@Component({
  template:  '<router-outlet></router-outlet>',
  directives: [RouterOutlet],
  providers:  [PageService]
})
@RouteConfig([
  {path:'/',         name: 'Pages', component: PageListComponent, useAsDefault: true},
  {path:'/:id',      name: 'PageDetail', component: PageDetailComponent}
])
export class PageComponent { }
