import {Component, OnInit, AfterViewInit, ElementRef}   from 'angular2/core';
import {Page, PageService} from './page.service';
//import {DataTables} from '../directives/datatables.directive';
import {Router, RouteParams} from 'angular2/router';

declare var $: any;
@Component({
  templateUrl: './app/pages/page-list.html',
  //directives: [DataTables]
})
export class PageListComponent implements OnInit, AfterViewInit {
  public pages: Page[];
  constructor(
    public pageService: PageService,
    public el: ElementRef,
    private _router: Router,
    routeParams: RouteParams) {
  }
  ngOnInit() {
    this.pageService.getPages().subscribe(
      pages => this.pages = pages,
      error => console.log('Could not load todos.')
    );
  }
  ngAfterViewInit(){
      console.log($('.datatable'));
    var that = this;
    setTimeout(function(){
      $(that.el.nativeElement).find('.datatable').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
      });
    }, 100);
  }
  onSelect(page: Page) {
    this._router.navigate( ['PageDetail', { id: page.id }] );
  }
}
