<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->string('financement');
            $table->string('age')->nullable();
            $table->string('puissance_fiscale')->nullable();
            $table->string('duree');
            $table->string('auto_financement');
            $table->string('telephone',8);
            $table->string('lien');
            $table->string('status');
            $table->string('prix');
            //$table->string('password', 60);
            //$table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
